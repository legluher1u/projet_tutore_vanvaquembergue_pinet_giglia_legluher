CREATE TABLE IF NOT EXISTS `partie` (
  `id_partie` int(11) NOT NULL AUTO_INCREMENT,
  `joueur1` text NOT NULL,
  `joueur2` text NOT NULL,
  `score1` int(11) NOT NULL,
  `score2` int(11) NOT NULL,
  `reprises` int(11) NOT NULL,
  PRIMARY KEY (`id_partie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;