<?php
require_once 'vendor/autoload.php';
require_once '/src/Controleur/ControleurScore.php';
require_once '/src/Controleur/ControleurEcran.php';

use controleur\ControleurScore as Controleur;
use controleur\ControleurEcran;

use Illuminate\Database\Capsule\Manager as DB;
ob_start();
$info=parse_ini_file("src/conf.ini");
$db = new DB();
$db->addConnection( [
 'driver' => $info["driver"],
 'host' => $info["host"],
 'database' => $info["database"],
 'username' => $info['username'],
 'password' => $info['password'],
 'charset' => 'utf8',
 'collation' => 'utf8_unicode_ci',
 'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

//$_SESSION['id_partie_courante'] = -1;
$app->get('/',function(){
 $cont = new Controleur();
 echo $cont->afficherAccueil();
})->name("accueil");

$app->get('/config',function(){
 $cont = new Controleur();
 echo $cont->afficherConfig();
})->name("config");

$app->post('/jeu',function(){
 $app = $app = \Slim\Slim::getInstance();
 $cont = new Controleur();
 echo $cont->commencerJeu($app->request->post('pseudo1'), $app->request->post('pseudo2'));
})->name("jeu");

$app->get('/jeu',function(){
 $cont = new Controleur();
 echo $cont->afficherJeu();
})->name("afficherjeu");


$app->post('/ajouterPoints/:nbplayer',function($nbplayer){
$app = $app = \Slim\Slim::getInstance();
 $cont = new Controleur();
 $cont->ajouterPoints($app->request->post('score'), $nbplayer);
});

$app->get('/finpartie',function(){
 $cont = new Controleur();
 echo $cont->afficherFin();
})->name('finpartie');

$app->get('/afficherScores', function() {
	$cont = new ControleurEcran();
	echo $cont->afficherScores();
});

$app->post('/majPartie/:id', function($id) {
	$app = $app = \Slim\Slim::getInstance();
	$cont = new Controleur();
	$cont->majPartie($id,$app->request->post('score1'),$app->request->post('score2'),$app->request->post('reprises'));
});

$app->get('/scoresCourants', function() {
	$cont = new ControleurEcran();
	$cont->getScoresCourants();
});

session_start();
$app->run();