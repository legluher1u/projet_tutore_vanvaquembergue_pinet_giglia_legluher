<?php
namespace controleur;
require_once '/src/Vue/VueScore.php';
require_once '/src/Modele/Partie.php';

use Vue\VueScore as Vue;
use Modele\Partie; 

define('AFF_ACCUEIL',1);
define('AFF_CONFIG',2);
define('AFF_JEU',3);
define('AFF_FIN',4);


class ControleurScore{
	
	public function afficherAccueil() {		
		$v = new Vue(NULL,AFF_ACCUEIL);
		return $v->render();
	}
	
	public function afficherConfig() {		
		$v = new Vue(NULL,AFF_CONFIG);
		return $v->render();
	}
	
	public function commencerJeu($p1, $p2) {
		$app = \Slim\Slim::getInstance();	
		unset($_SESSION['scores']);
		unset($_SESSION['pseudo1']);
		unset($_SESSION['pseudo2']);
		$_SESSION['pseudo1'] = $p1;
		$_SESSION['pseudo2'] = $p2;
		$partie = new Partie();
		$partie->joueur1 = $p1;
		$partie->joueur2 = $p2;
		$partie->score1 = 0;
		$partie->score2 = 0;
		$partie->reprises = 0;
		$partie->save();
		$_SESSION['id_partie_courante'] = Partie::max('id_partie');
		$app->redirect($app->urlFor('afficherjeu'));
	}
	
	public function afficherJeu() {	
		$v = new Vue(NULL,AFF_JEU);
		return $v->render();
	}
	
	public function ajouterPoints($p, $nb) {
		$app = \Slim\Slim::getInstance();	
		var_dump($p);
		var_dump($nb);
		if(isset($_SESSION['scores'][$nb])) {
			$_SESSION['scores'][$nb] += $p;
		}else {
			$_SESSION['scores'][$nb] = $p;
		}
		$app->redirect($app->urlFor('afficherjeu'));
	}
	public function afficherFin() {	
		if(isset($_SESSION['id_partie_courante']) && $_SESSION['id_partie_courante'] !=-1){
			$partie = Partie::where('id_partie','=',$_SESSION['id_partie_courante'])->first();
			$partie->estFinie=true;
			$partie->save();
			$v = new Vue($partie,AFF_FIN);
			return $v->render();
		}else {
			return "Cette partie n'existe pas";
		}
		
	}
	
	public function majPartie($id,$s1,$s2,$r) {
		$partie = Partie::where('id_partie','=',$id)->first();
		$partie->score1 = $s1;
		$partie->score2 = $s2;
		$partie->reprises = $r;
		$partie->save();
	}
}