<?php
namespace controleur;
require_once '/src/Vue/VueEcran.php';
require_once '/src/Modele/Partie.php';
use Vue\VueEcran;


use Vue\VueScore as Vue;
use Modele\Partie; 

define('AFF_SCORES',1);
define('AFF_ATTENTE',2);


class ControleurEcran{
	
	public function afficherScores() {
		if(isset($_SESSION['id_partie_courante']) && $_SESSION['id_partie_courante'] != -1){
			$partie = Partie::where('id_partie','=',$_SESSION['id_partie_courante'])->first();
			$v = new VueEcran($partie,AFF_SCORES);
			return $v->render();
		}else {
			$v = new VueEcran(NULL,AFF_ATTENTE);
			return $v->render();
		}
	}
	
	public function getScoresCourants() {
		$partie = Partie::where('id_partie','=',$_SESSION['id_partie_courante'])->first();
		$j1 = $partie->joueur1;
		$j2 = $partie->joueur2;
		$s1 = $partie->score1;
		$s2 = $partie->score2;
		$r = $partie->reprises;
		$arr = array('joueur1' => $j1, 'joueur2' => $j2, 'score1' => $s1, 'score2' => $s2, 'reprises' => $r);
		echo json_encode($arr);
	}
}