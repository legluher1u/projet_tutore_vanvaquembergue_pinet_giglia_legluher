<?php 
namespace modele;


class Partie extends \Illuminate\Database\Eloquent\Model{
	
	protected $table="partie";
	protected $primaryKey = 'id_partie' ;
	public $timestamps = false;
}