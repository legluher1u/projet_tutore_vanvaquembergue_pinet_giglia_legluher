<?php 

namespace Vue;

class VueEcran{
	public $partie;
	public $aff;
	

	public function __construct($p, $affichage){
		$this->partie=$p;
		$this->aff=$affichage;
	}
	
	private function afficherScores() {
		$p = $this->partie;
		$res = '<h2>Scores</h2>
				<div class="row" style="height:10%;">
					<div class="col-xs-5" id="joueur1">'.$p->joueur1.'</div>
					<div class="col-xs-2" id="reprises">'.$p->reprises.'</div>
					<div class="col-xs-5" id="joueur2">'.$p->joueur2.'</div>
				</div>
				<div class="row">
					<div class="col-xs-6" id="score1">'.$p->score1.'</div>
					<div class="col-xs-6" id="score2">'.$p->score2.'</div>
				</div>';
		return $res;
	}
	
	private function afficherAttente(){
		echo "Aucune partie en cours";
	}
	
	public function render(){
		switch($this->aff){
			case AFF_SCORES:
			$content = $this->afficherScores();
			$racine="";
			break;
			case AFF_ATTENTE:
			$content = $this->afficherAttente();
			$racine="";
			break;
			default:
			$content = $this->afficherAttente();
			$racine="";
			break;
		}
		
		
		$html =<<<END
<!DOCTYPE html>
<html>
<head> 
	<title>Billard</title> 
	<link href="{$racine}css/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
	<link rel="stylesheet" media="all" type="text/css" href="{$racine}css/style.css"/>
</head>
<body>
	<div class="container-fluid">
	{$content}
	</div>
	<script type="text/javascript" src="{$racine}js/jquery.min.js"></script> 
	<script type="text/javascript" src="{$racine}js/ecran.js"></script> 

</body>
<html>
END;
		return $html;
	}
}