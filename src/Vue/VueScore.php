<?php 

namespace Vue;

class VueScore{
	public $partie;
	public $aff;
	

	public function __construct($p, $affichage){
		$this->partie=$p;
		$this->aff=$affichage;
	}
	
	private function afficherAccueil(){

		$app = \Slim\Slim::getInstance();
		$a= $app->urlFor('config');
		$res = '<h1> Appuyer sur ce bouton pour lancer la partie </h1></br>';
		$res.='<form method="get" action='.$a.'> 
					<button type="submit">Commencer une partie</button>
					</form>';

		return $res;
	}
	
	private function afficherConfig(){
		$app = \Slim\Slim::getInstance();

		$res = '<h1>Configurez votre partie</h1><hr/>
		<form action="'.$app->urlFor("jeu").'" method="post">
        <label for="pseudo1">Pseudo joueur 1</label> : <input type="text" name="pseudo1" id="pseudo1" required/><br/>
	    <label for="pseudo2">Pseudo joueur 2</label> : <input type="text" name="pseudo2" id="pseudo2" required/><br/>
		<button type="submit">Lancer la partie</button>
		</form>
		';
		return $res;
	}
	
	private function afficherJeu(){
		$app = \Slim\Slim::getInstance();
		if(isset($_SESSION['id_partie_courante']) && $_SESSION['id_partie_courante'] != -1) {
			$id = $_SESSION['id_partie_courante'];
		}else {
			$id = 1;
		}
		
		/*<div class="row">
			<div class="col-xs-12">id partie:'.$id.'</div>
		</div>*/
		$res =	'<div class="row">
					<div class="col-xs-5" id="joueur1">'.$_SESSION['pseudo1'].'</div>
					<div class="col-xs-2" id="reprises">0</div>
					<div class="col-xs-5" id="joueur2">'.$_SESSION['pseudo2'].'</div>
				</div>
				<div class="row">
					<div class="col-xs-5" id="score1">0</div>
					<div class="col-xs-5 col-xs-offset-2" id="score2">0</div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-xs-offset-3">
						<div class="row">
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave11" onClick="ajouter(1)">1</button></div>
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave12" onClick="ajouter(2)">2</button></div>
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave13" onClick="ajouter(3)">3</button></div>
						</div>
						<div class="row">
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave14" onClick="ajouter(4)">4</button></div>
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave15" onClick="ajouter(5)">5</button></div>
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave16" onClick="ajouter(6)">6</button></div>
						</div>
						<div class="row">
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave17" onClick="ajouter(7)">7</button></div>
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave18" onClick="ajouter(8)">8</button></div>
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave19" onClick="ajouter(9)">9</button></div>
						</div>
						<div class="row">
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave1-" onClick="signe(-1)">-</button></div>
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave10" onClick="ajouter(0)">0</button></div>
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave1+" onClick="signe(1)">+</button></div>
						</div>
						<div class="row">
							<div class="col-xs-4"><button class="btn btn-lg btn-default" id="pave1Eff" onClick="effacer(1)">Effacer</button></div>
							<div class="col-xs-8"><button class="btn btn-lg btn-default" id="pave1Val" onClick="valider()">Valider</button></div>
						</div>
						<div class="row">
							<div class="col-xs-12" id="ajout">0</div>
						</div>
					</div>	
				</div>
				<a href="'.$app->urlFor("finpartie").'">Finir la partie</a>';
		
		return $res;
	}
	
	private function afficherFin(){
		$app = \Slim\Slim::getInstance();
		$p = $this->partie;
		$res = '<h1>Fin de partie</h1><hr/>
		Joueur 1 : '.$p->joueur1.'<br/>
		Score joueur 1: '.$p->score1.'<br/>
		<br/>
		Joueur 2 : '.$p->joueur2.'<br/>
		Score joueur 2: '.$p->score2.'<br/>
		<a href="'.$app->urlFor("config").'">Rejouer</a>
		<a href="'.$app->urlFor("accueil").'">Accueil</a>';
		

		return $res;
	}
	
	public function render(){
		switch($this->aff){
			case AFF_ACCUEIL:
			$content = $this->afficherAccueil();
			$racine="";
			break;
			case AFF_CONFIG:
			$content = $this->afficherConfig();
			$racine="";
			break;
			case AFF_JEU:
			$content = $this->afficherJeu();
			$racine="";
			break;
			case AFF_FIN:
			$content = $this->afficherFin();
			$racine="";
			break;
			default:
			$content = $this->afficherAccueil();
			$racine="";
			break;
		}
		
		if(isset($_SESSION['id_partie_courante']) && $_SESSION['id_partie_courante'] != -1) {
			$id = $_SESSION['id_partie_courante'];
		}else {
			$id = 1;
		}
		
		$html =<<<END
<!DOCTYPE html>
<html>
<head> 
	<title>Billard</title> 
	<link href="{$racine}css/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
	<link href="{$racine}css/Bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" media="all" type="text/css">
	<link rel="stylesheet" media="all" type="text/css" href="{$racine}css/style.css"/>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
</head>
<body onload=initialise({$id})>
	<div class="container-fluid">
	{$content}
	</div>
<script type="text/javascript" src="{$racine}js/jquery.min.js"></script> 
<script type="text/javascript" src="{$racine}js/pave.js"></script> 
</body>
<html>
END;
		return $html;
	}
}