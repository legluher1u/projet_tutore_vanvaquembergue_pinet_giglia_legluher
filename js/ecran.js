$(document).ready(
    function() {
        setInterval(function() {
			var xhr = getXMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
					var obj = JSON.parse(xhr.responseText);

					document.getElementById("reprises").innerHTML = obj.reprises;
					document.getElementById("joueur1").innerHTML = obj.joueur1;
					document.getElementById("joueur2").innerHTML = obj.joueur2;
					document.getElementById("score1").innerHTML = obj.score1;
					document.getElementById("score2").innerHTML = obj.score2;

				}
			};
			xhr.open("GET", "scoresCourants", true);
			xhr.send(null);
	}, 1000);
});
			
function getXMLHttpRequest() {
	var xhr = null;
	
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest(); 
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}
	
	return xhr;
}