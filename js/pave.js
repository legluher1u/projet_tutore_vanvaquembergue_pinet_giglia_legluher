var ajout = 0;

var id_partie_courante = 1;
var joueur_courant = 1;
var couleur_non_selectionne = "aliceblue";
var couleur_selectionne = "#ffb3b3";

$(document).ready(
    function() {
        setInterval(function() {
			var xhr = getXMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
					var obj = JSON.parse(xhr.responseText);

					document.getElementById("reprises").innerHTML = obj.reprises;
					document.getElementById("joueur1").innerHTML = obj.joueur1;
					document.getElementById("joueur2").innerHTML = obj.joueur2;
					document.getElementById("score1").innerHTML = obj.score1;
					document.getElementById("score2").innerHTML = obj.score2;

				}
			};
			xhr.open("GET", "scoresCourants", true);
			xhr.send(null);
	}, 1000);
	document.getElementById("joueur"+joueur_courant).style.backgroundColor = couleur_selectionne;
	document.getElementById("score"+joueur_courant).style.backgroundColor = couleur_selectionne;
});

function initialise(id) {
	id_partie_courante = id;
	var xhr = getXMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			var obj = JSON.parse(xhr.responseText);
			document.getElementById("reprises").innerHTML = obj.reprises;
			document.getElementById("joueur1").innerHTML = obj.joueur1;
			document.getElementById("joueur2").innerHTML = obj.joueur2;
			document.getElementById("score1").innerHTML = obj.score1;
			document.getElementById("score2").innerHTML = obj.score2;
		}
	};
	xhr.open("GET", "scoresCourants", true);
	xhr.send(null);
	document.getElementById("joueur"+joueur_courant).style.backgroundColor = couleur_selectionne;
	document.getElementById("score"+joueur_courant).style.backgroundColor = couleur_selectionne;
}

function ajouter(n) {
	
	ajout *= 10;
	ajout += n;
	document.getElementById("ajout").innerHTML = ajout;
}

function signe(s){
	if(s === 1){
		ajout = Math.abs(ajout);
	}else if(s===-1){
		ajout = -Math.abs(ajout);
	}
	
	document.getElementById("ajout").innerHTML = ajout;
}

function valider() {
	document.getElementById("joueur"+joueur_courant).style.backgroundColor = couleur_non_selectionne;
	document.getElementById("score"+joueur_courant).style.backgroundColor = couleur_non_selectionne;


	if(joueur_courant===1) {
		if(ajout > 0) {
			document.getElementById("reprises").innerHTML = parseInt(document.getElementById("reprises").innerHTML) + 1;
		}
		document.getElementById("score1").innerHTML = parseInt(document.getElementById("score1").innerHTML) + ajout;
		ajout = 0;
		joueur_courant = 2;
	}else if(joueur_courant === 2){
		document.getElementById("score2").innerHTML = parseInt(document.getElementById("score2").innerHTML) + ajout;
		ajout = 0;
		joueur_courant = 1;
	}
	document.getElementById("ajout").innerHTML = 0;
	document.getElementById("joueur"+joueur_courant).style.backgroundColor = couleur_selectionne;
	document.getElementById("score"+joueur_courant).style.backgroundColor = couleur_selectionne;

	var xhr = getXMLHttpRequest();
	
	
	var score1 = parseInt(document.getElementById("score1").innerHTML);
	var score2 = parseInt(document.getElementById("score2").innerHTML);
	var reprises = parseInt(document.getElementById("reprises").innerHTML);
	xhr.open("POST", "majPartie/"+id_partie_courante, true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			//ici rien besoin de faire, il n'y a pas de retour
		}
	};
	xhr.send("score1="+score1+"&score2="+score2+"&reprises="+reprises);
}

function effacer() {
		ajout = 0;
	
	document.getElementById("ajout").innerHTML = 0;
}

function getXMLHttpRequest() {
	var xhr = null;
	
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest(); 
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}
	
	return xhr;
}